require 'rails_helper'

feature 'user signs up' do
  scenario 'successfully' do
    visit root_path
    expect(page).to have_css 'a', text: 'Sign up'
    click_on 'Sign up'
    fill_in 'user_email', with: Faker::Internet.email
    password = Faker::Internet.password
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password
    expect { click_button 'Sign up' }.to change { User.count }.by(1)
  end
end
