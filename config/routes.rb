Rails.application.routes.draw do
  root to: 'schedule#index'
  devise_for :users
end
