class Schedule
  attr_accessor :calendars
  attr_accessor :upcoming

  def initialize(calendars:, upcoming:)
    @calendars = calendars
    @upcoming = upcoming
  end
end
